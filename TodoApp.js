
/* ---------------------------------
TodoApp
--------------------------------- */

export default class TodoApp {
  /*
    basic outline

    data
      todo store

    methods
      add task
      remove task
      edit task
      persist
  */

  // data
  store = []


  // find
  findTask(id) {
    const numberId = parseInt(id);
    return this.store.findIndex(
      (el) => el.id === numberId
    )
  }

  // 
  logStore() { console.table(this.store) }


  // add
  addTask(content) {
    const { length } = this.store;

    this.store.push(content);

    console.log(this.store, length);
  }


  // mark completion
  markCompletion(id) {
    const index = this.findTask(id);

    this.store[index]['completed'] ?
      this.store[index]['completed'] = false
      :
      this.store[index]['completed'] = true

    // console.log(this.store[index])
    
    console.log(`marked task with id ${id} as complete/incomplete`)
  }


  // edit
  editTask(id, content) {
    const index = this.findTask(id);
    this.store[index]['content'] = content;
    
    console.log(`edited task with id ${id}`)
    // this.logStore();
  }


  // delete
  deleteTask(id) {
    const index = this.findTask(id);    
    this.store.splice(index, 1);
    const { length } = this.store;
    
    console.log(`deleted task with id ${id}`, length)
    // this.logStore();
  }
}
