
/* ---------------------------------
index.js
--------------------------------- */

import TodoItem from './TodoItem';
import TodoApp from './TodoApp';

const App = new TodoApp;


// init
window.onload = (e) => {
  const d = new Date;
  console.log('loaded @', d.toLocaleTimeString());

  const
    form = document.querySelector('#taskForm'),
    input = form.elements.taskInputMain,
    submit = form.elements.taskInputMainSubmit,
    taskList = document.querySelector('.taskList')
  ;


  // input form listener
  submit.addEventListener('click', function(e) {
    e.preventDefault();

    const value = input.value;
    const task = new TodoItem(value);
    
    App.addTask(task);
    renderTasks(App.store)

    form.reset();
  });
}


// renderTasks
function renderTasks(store) {
  const length = store.length;
  const taskList = document.querySelector('.taskList');

  document.querySelector('.taskList').innerHTML = '';

  if (length === 0) {
    taskList.insertAdjacentHTML(
      'afterbegin',
      `<li>No tasks! Create one &uarr;</li>`
    );
  } else if (length > 0) {
    for (const item of store) {
      createTaskUI(item)
    }
  }
}


// createTaskUI
const createTaskUI = (task) => {
  const taskList = document.querySelector('.taskList');

  const template = `
  <li
    id=${task.id}
    class="task-container"
    data-completed="${task.completed}"
  >
    <input
      type="checkbox"
      ${task.completed ? 'checked' : ''}
      id="taskItemCompletion"
      class="task-checkbox"
    >

    <span
      class="task-content"
      contenteditable="false"
    >
      ${task.content}
    </span>

    <span class="task-timestamp">
      ${task.timestamp}
    </span>

    <div class="task-controls">
      <button
        type="button"
        class="task-button--edit"
        id="taskItemEdit"
      >
        Edit
      </button>

      <button
        type="button"
        class="task-button--delete"
        id="taskItemDelete"
      >
        Delete
      </button>
    </div>
  </li>
  `;

  taskList.insertAdjacentHTML('afterbegin', template);

  taskList.firstElementChild.addEventListener(
    'click',
    handleTaskActions
  );
}


// handleTaskActions
function handleTaskActions(e) {
  e.preventDefault();

  const id = e.currentTarget.id;
    
  switch (e.target.className) {

    // 'task-checkbox'
    case 'task-checkbox':
      console.log(id, 'checkbox');
      const { completed } = this.dataset;

      completed === 'false' ?
        this.dataset.completed = 'true'
        :
        this.dataset.completed = 'false'

      App.markCompletion(id);
      renderTasks(App.store);
      
      break;


    // 'task-button--edit'
    case 'task-button--edit':
      console.log('button')

      const container = this.querySelector('.task-content');

      /*
        TODO
        when clicked, remove contenteditable
        and restore button label
      */
      e.target.innerText = 'Done';

      container.setAttribute('contenteditable', true);
      container.addEventListener(
        'input',
        function(e) {
          // console.log(this.innerText)
          // console.log(e.data)
          const newValue = this.innerText;
          App.editTask(id, newValue);
        }
      )

      break;


    // 'task-button--delete'
    case 'task-button--delete':
      App.deleteTask(id);
      renderTasks(App.store)

      break;


    // default
    default:
      console.log('default');

      break;
  }
}
