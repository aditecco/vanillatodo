
/* ---------------------------------
TodoItem
--------------------------------- */

export default class TodoItem {
  /*
    basic outline

    data
      todo item

    methods
      …
  */

  // counter = 0

  constructor(content, completed = false) {
    const d = new Date();

    this.content = content
    this.completed = completed
    this.id = Date.now()
    this.timestamp = d.toLocaleString()
  }
}
